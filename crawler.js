const cheerio = require("cheerio")
const axios = require("axios")
const fs = require('fs')
const [X, Y, START_URL, MAX_DEPTH] = process.argv

async function recFn (sourceUrl, result, depth, origin, allPages) {
    try {
        const page = await axios.get(sourceUrl)
        const $ = cheerio.load(page.data)

        const images = $('img').map(function () {
            return $(this).attr('src')
        })

        for (let image of images) {
            result.push({ imageUrl: image, sourceUrl: sourceUrl, depth })
        }

        const links = $('a').map(function () {
            return $(this).attr('href')
        })
        //console.log({ depth, linksCount: links.length, imagesCount: images.length })

        for (let link of links) {
            let newOrigin = origin
            if (!link.includes('https')) {
                link = newOrigin + link
            } else {
                newOrigin = new URL(link).origin
            }
            if (depth < Number(MAX_DEPTH) && !allPages.has(link)) {
                allPages.add(link)
                await recFn(link, result, depth+1, newOrigin)
            }
        }
    } catch (e) {
        console.error('recFn broke: ', sourceUrl)
    }
}

async function init () {
    let result = []
    let allPages = new Set([START_URL])
    const origin = new URL(START_URL).origin

    await recFn(START_URL, result, 0, origin, allPages)
    //console.log(allPages)

    fs.writeFile('./result.json', JSON.stringify({ results: result }, null, 4), 'utf8', function (e) {
        if (e) {
            console.error('Broke while writing result to file: ', e)
        } else {
            console.log("File is ready :) ")
        }
    })
}
init()